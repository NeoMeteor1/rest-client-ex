<?php
//API Url
$url = 'homestead.app/api/v1/areas/4';

//Initiate cURL.
$ch = curl_init($url);

//The JSON data.
$jsonData = array(
    'name' => 'API Client Test Two',
    'district_id' => 0,
    'type' => 'business',
    'grid_x' => '3',
    'grid_y' => '5',
);

//Encode the array into JSON.
$jsonDataEncoded = json_encode($jsonData);

//Tell cURL that we want to send a PATCH request.
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');

//Attach our encoded JSON string.
curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

//Set the content type to application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'X-Requested-With: XMLHttpRequest',
    'Authorization: Bearer 4oJOs9ft3wuDPiivBNQc2tLQ3hguIsZWXxJTkl3AOT2aha3gQ98YSYfZUW2m',
));

//Execute the request
$result = curl_exec($ch);
