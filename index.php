<?php
//API Url
$url = 'homestead.app/api/v1/areas';

//Initiate cURL.
$ch = curl_init($url);

//Tell cURL that we want to send a GET request.
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

//Set the content type to application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'X-Requested-With: XMLHttpRequest',
    'Authorization: Bearer 4oJOs9ft3wuDPiivBNQc2tLQ3hguIsZWXxJTkl3AOT2aha3gQ98YSYfZUW2m',
));

//Execute the request
$result = curl_exec($ch);
